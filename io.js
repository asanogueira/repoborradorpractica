const fs = require("fs");

function writeUserDataToFile(data){

    console.log("writeUserDataToFile");

    // transformacion de datos a json, si no estaria guardando el objeto
    var jsonUserData = JSON.stringify(data);

    fs.writeFile("./usuarios.json", jsonUserData, "utf8",
      function(error){
          if(error){
            console.log(error);
          } else {
            console.log("Usuario persistido");
          }
      }

    );
}

module.exports.writeUserDataToFile = writeUserDataToFile;
