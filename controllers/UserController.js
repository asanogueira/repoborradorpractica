const io = require('../io');
const requestJson = require('request-json');
const crypt = require('../crypt');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuasn9ed/collections/";
//const mLabAPIKey = "apiKey=V0UWtDtX6fLRI9T-boCVx72eBI-73VcG";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getUsersV1(req, res){

    console.log('GET /apitechu/v1/users');

    var users = require('../usuarios.json');
    var resultado ={};

    /*  if(req.query.$top){

          resultado.users=users.slice(0, req.query.$top)
          if(req.query.$count){
            resultado.total = users.length;
          }
    } else {
        if(req.query.$count){
          resultado.total = users.length;
          resultado.users = users;
        }
        else
          resultado = users;
    }*/
    req.query.$top ? (resultado.users=users.slice(0, req.query.$top)) : (resultado.users = users);
    if(req.query.$count && req.query.$count=='true')
          resultado.total = users.length;

    res.send(resultado);
}

function getUsersV2(req, res){

    console.log('GET /apitechu/v2/users');

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");

    httpClient.get("user?" + mLabAPIKey,
      function (err, resMLab, body){
        var response = !err ? body : {"msg" : "Error obteniendo los usuarios"}

        res.send(response);
      }
    );
}

function getUserByIdV2(req, res){

    console.log('GET /apitechu/v2/users/:id');

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");

    var consulta = 'q={"id":' + req.params.id + "}";
    console.log(consulta);

    httpClient.get("user?" + consulta + "&" + mLabAPIKey,
      function (err, resMLab, body){
        //var response = !err ? body : {"msg" : "Error obteniendo los usuarios"}
        if(err){
          var response = {"msg" : "Error obteniendo los usuarios"};
          res.status(500);
        } else {
            if(body.length > 0){
              var response = body [0];
            } else {
              var response = {"msg" : "Usuario no encontrado"};
              res.status(404);
            }
        }
        res.send(response);
      }
    );
}

function createUserV1(req, res){

  console.log('POST /apitechu/v1/users');
  console.log(req.body.first_name);

  var newUser = {
    /*"first_name": req.headers.first_name,
    "last_name": req.headers.last_name,
    "email": req.headers.email*/
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  var users = require('../usuarios.json');
  users.push(newUser);

  console.log("Usuario añadido");
  io.writeUserDataToFile(users);

}

function createUserV2(req, res){

  console.log('POST /apitechu/v2/users');

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  };
  //console.log("datos del usuario " + JSON.stringify(newUser));

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKey, newUser,
    function (err, resMLab, body){
      // var response = !err ? {"msg" : "Usuario creado con éxito"} : {"msg" : "Error creando usuario"}
      // res.send(response);
      console.log("Usuario guardado con éxito");
      res.status(201).send({"msg" : "Usuario creado con éxito"});
    }
  );


}


 function deleteUserV1(req, res) {
    console.log("DELETE /apitechu/v1/users/:id");

    console.log("id es " + req.params.id);
    var users = require('../usuarios.json');

    var deleted = false;

    console.log("Usando for normal");

    for (var i = 0; i < users.length; i++) {

       console.log("comparando " + users[i].id + " y " +  req.params.id);

       if (users[i].id == req.params.id) {

         console.log("La posicion " + i + " coincide");

         users.splice(i, 1);

         deleted = true;

         break;

       }

    }

    if(deleted) {
       io.writeUserDataToFile(users);
    }

    var msg = deleted ? "Usuario borrado" : "Usuario no encontrado." ;
    console.log(msg);

    res.send({"ms2" : msg});

}


module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
//module.exports.deleteUserV2 = deleteUserV2;
