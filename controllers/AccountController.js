const io = require('../io');
const requestJson = require('request-json');
const crypt = require('../crypt');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuasn9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;



/**function getUsersV2(req, res){

    console.log('GET /apitechu/v2/users');

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");

    httpClient.get("user?" + mLabAPIKey,
      function (err, resMLab, body){
        var response = !err ? body : {"msg" : "Error obteniendo los usuarios"}

        res.send(response);
      }
    );
}*/

function getAccountByUserIdV1(req, res){

    console.log('GET /apitechu/v1/account/:id');

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");

    var consulta = 'q={"userid":' + req.params.id + "}";
    console.log(consulta);

    httpClient.get("account?" + consulta + "&" + mLabAPIKey,
      function (err, resMLab, body){
        //var response = !err ? body : {"msg" : "Error obteniendo los usuarios"}
        if(err){
          var response = {"msg" : "Error obteniendo las cuentas"};
          res.status(500);
        } else {
            if(body.length > 0){
              //console.log(body[0]);
              var response = body;
            } else {
              var response = {"msg" : "El usuario no tiene cuentas"};
              res.status(404);
            }
        }
        res.send(response);
      }
    );
}






module.exports.getAccountByUserIdV1 = getAccountByUserIdV1;
