const io = require('../io');
const requestJson = require('request-json');
const crypt = require('../crypt');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuasn9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function loginUserV1(req, res){

    console.log('POST /apitechu/v1/login');

    var validacion = false;
    var id = 0;
    console.log("Usuario = " + req.body.email +" contraseña = " +req.body.password);

    var users = require('../usuarios.json');

    for (var i=0; i<users.length; i++){

      if(users[i].email == req.body.email && users[i].password == req.body.password){
        validacion = true;
        console.log("Usuario y password correctas");
        users[i].logged = true;
        id = users[i].id;
        break;
      }
    }

    var resp = {};

    if(validacion ) {
      console.log("Usuario modificado");
      io.writeUserDataToFile(users);
      resp.mensaje = "Login correcto";
      resp.idUsuario = id;
    }
    else {
      console.log("Usuario / password no existen o son incorrectas");
      resp.mensaje = "Login incorrecto";
    }

    res.send(resp);

}

function loginUserV2(req, res){

    console.log('POST /apitechu/v2/login');

    var validacion = false;
    var id = 0;
    var resp = {};

    console.log("Usuario = " + req.body.email +" contraseña = " +req.body.password);

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");
    var consulta = 'q={"email":"' + req.body.email + '"}';
    console.log("Consulta: "+ consulta);

    httpClient.get("user?" + consulta + "&" + mLabAPIKey,
      function (err, resMLab, body){
        if(err){
          console.log("Error consultando el usuario");
          resp.mensaje = "Error en la consulta";
          res.send(resp);
          res.status(500);
        } else {
          //if(body.length > 0 && req.body.password == body[0].password){
          if(body.length > 0 && crypt.checkPassword(req.body.password, body[0].password)){
            validacion = true;
            id = body[0].id;
            console.log("CONTRASEÑA CORRECTA, id usuario = "+ id);
          } else {
            console.log("Email no encontrado o contraseña incorrecta");
            resp.mensaje = "Email no encontrado o contraseña incorrecta";
            res.send(resp);
            res.status(401);
          }
        }

        if(validacion){
          console.log("entramos al put");
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + consulta + "&" + mLabAPIKey, JSON.parse(putBody),
            function (errPUT, resMLabPUT, bodyPUT){
              if(!errPUT){
                console.log("LOGIN REALIZADO "+id);
                resp.mensaje = "Login correcto";
                resp.idUsuario = id;

              } else {
                resp.mensaje = "Error actualizando";
                res.status(500);
              }
              res.send(resp);
          }
        )
        }
      }
    )

  }

function logoutUserV1(req, res){

    console.log('POST /apitechu/v1/logout');
    console.log('id a borrar '+req.params.id);
    var validacion = false;
    var users = require('../usuarios.json');

    for (var i=0; i<users.length; i++){

      if(users[i].id == req.params.id && users[i].logged){
        validacion = true;
        console.log("Localizado usuario " + req.params.id);
        delete users[i].logged;
        console.log("logout realizado");
        break;
      }
    }

    var msg = "Logout incorrecto";

    if(validacion ) {
      io.writeUserDataToFile(users);
      msg = "Logout correcto";
    }

    res.send({"mensaje" : msg});
}

function logoutUserV2(req, res){

    console.log('POST /apitechu/v2/logout/:id' + req.params.id);
    var validacion = false;
    var resp = {};

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");
    var consulta = 'q={"id":' + req.params.id + '}';
    console.log("Consulta: "+ consulta);

    httpClient.get("user?" + consulta + "&" + mLabAPIKey,
      function (err, resMLab, body){
        if(err){
          console.log("Error consultando el usuario");
          resp.mensaje = "Error en la consulta";
          res.send(resp);
          res.status(500);
        } else {
          if(body.length > 0 && body[0].logged === true){
            validacion = true;
            console.log("EL usuario está logado, id usuario = "+ req.params.id);
          } else {
            console.log("Usuario incorrecto o no logado");
            resp.mensaje = "Usuario incorrecto o no logado";
            res.send(resp);
            res.status(401);
          }
        }

        if(validacion){
          console.log("entramos al put");
          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + consulta + "&" + mLabAPIKey, JSON.parse(putBody),
            function (errPUT, resMLabPUT, bodyPUT){
              if(!errPUT){
                console.log("LOGOUT REALIZADO "+ req.params.id  );
                resp.mensaje = "Logout correcto";

              } else {
                resp.mensaje = "Error realizando logout";
                res.status(500);
              }
              res.send(resp);
          }
        )
        }
      }
    )
}

module.exports.loginUserV1 = loginUserV1;
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUserV1 = logoutUserV1;
module.exports.logoutUserV2 = logoutUserV2;
