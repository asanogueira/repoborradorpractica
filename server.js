require('dotenv').config();

const express = require('express');
// Inicializamos el framework:
const app =express();

const io = require('./io');

const userController = require('./controllers/UserController');

const authController = require('./controllers/AuthController');

const accountController = require('./controllers/AccountController');

var enableCORS = function (req, res, next){
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");

  next();
}

const port = process.env.PORT || 3000;

app.use(express.json());
app.use (enableCORS);

app.listen (port);
console.log("API escuchando  en el puerto " + port );

app.get('/apitechu/v1/account/:id', accountController.getAccountByUserIdV1);

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);

app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);

app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);

//POST a /apitechu/v1/login
app.post('/apitechu/v1/login', authController.loginUserV1);
app.post('/apitechu/v2/login', authController.loginUserV2);

//POST a /apitechu/v1/logout
app.post('/apitechu/v1/logout/:id', authController.logoutUserV1);
app.post('/apitechu/v2/logout/:id', authController.logoutUserV2);

app.get ('/apitechu/v1/hello',
  function (req, res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg": "Hola desde API TechU"});
  }
);

// app.post('/apitechu/v1/monstruo/:p1/:p2',
//
//       function(req, res){
//           console.log("Query String");
//           console.log(req.query);
//
//           console.log("Headers");
//           console.log(req.headers);
//
//           console.log("Body");
//           console.log(req.body);
//       }
// );

//app.get('/apitechu/v1/users',
//function (req, res){
//  console.log('GET /apitechu/v1/users');

  //res.sendFile('usuarios.json', {root: __dirname});
//  var users = require('./usuarios.json');
  // ALTERNATIVA res.sendFile('usuarios.json', {root: __dirname});
//  res.send(u ALTERNATIVA sers);
//}

//);
